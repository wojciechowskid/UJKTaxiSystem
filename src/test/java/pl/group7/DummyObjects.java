package pl.group7;

import pl.group7.model.data.Role;
import pl.group7.model.data.State;
import pl.group7.model.entity.Order;
import pl.group7.model.entity.Place;
import pl.group7.model.entity.ServiceType;
import pl.group7.model.entity.User;
import pl.group7.model.to.OrderTO;
import pl.group7.model.to.UserTO;

import java.util.Collections;
import java.util.Date;

/**
 * Created by allst on 05.05.2017.
 */
public class DummyObjects {

    public static User getDummyUser() {
        User user = new User();
        user.setId(1);
        user.setName("Dominik");
        user.setSurname("Wojciechowski");
        user.setLogin("Dominik");
        user.setPassword("Wojciechowski");
        user.setBirthDay(new Date());
        user.setJoinDate(new Date());
        user.setLastLoginDate(new Date());
        user.setMailAddress("admin@admin.pl");
        user.setPhoneNumber("8546548646");
        user.setRole(Collections.singletonList(Role.ADMIN));
        user.setOrdersMade(Collections.singletonList(new Order()));
        user.setOrdersDispatched(Collections.singletonList(new Order()));
        user.setOrdersAssigned(Collections.singletonList(new Order()));
        return user;
    }

    public static UserTO getDummyUserTO() {
        UserTO user = new UserTO();
        user.setId(1);
        user.setName("Dominik");
        user.setSurname("Wojciechowski");
        user.setLogin("Dominik");
        user.setPassword("Wojciechowski");
        user.setBirthDay(new Date());
        user.setJoinDate(new Date());
        user.setLastLoginDate(new Date());
        user.setMailAddress("admin@admin.pl");
        user.setPhoneNumber("8546548646");
        user.setRoleList(Collections.singletonList(Role.ADMIN));
        user.setOrdersMade(Collections.singletonList(1L));
        user.setOrdersDispatched(Collections.singletonList(1L));
        user.setOrdersAssigned(Collections.singletonList(1L));
        return user;
    }

    public static User getModifiedUser() {
        User secondUser = getDummyUser();
        secondUser.setPassword("Ala");
        secondUser.setName("Dominika");
        return secondUser;
    }
    public static UserTO getModifiedUserTO() {
        UserTO secondUser = getDummyUserTO();
        secondUser.setPassword("Ala");
        secondUser.setName("Dominika");
        return secondUser;
    }

    public static User getAnotherUser() {
        User secondUser = getDummyUser();
        secondUser.setId(1L);
        secondUser.setLogin("ALA");
        return secondUser;
    }

    public static Order getDummyOrder(){
        Order order = new Order();
        order.setId(1L);
        order.setOrderDate(new Date());
        order.setDestination(getDummyPlace());
        order.setSource(getDummyPlace());
        order.setState(State.IN_PROCESS);
        order.setDistance(22.5);
        order.setCustomer(new User());
        order.getCustomer().setId(1);
        order.setConsultant(new User());
        order.getConsultant().setId(1);
        order.setDriver(new User());
        order.getDriver().setId(1);
        order.setServiceType(getDummyServiceType());
        return order;
    }

    public static OrderTO getDummyOrderTO(){
        OrderTO order = new OrderTO();
        order.setId(1L);
        order.setOrderDate(new Date());
        order.setDistance(22.5);
        order.setDestination(getDummyPlace());
        order.setSource(getDummyPlace());
        order.setCustomer(1L);
        order.setConsultant(1L);
        order.setDriver(1L);
        order.setServiceType(1L);
        return order;
    }

    public static Order getModifiedOrder(){
        Order order = getDummyOrder();
        order.setState(State.FINISHED);
        order.getCustomer().setLogin("Dominik");
        return order;
    }

    public static OrderTO getModifiedOrderTO(){
        OrderTO order = getDummyOrderTO();
        order.setState(State.FINISHED);
        return order;
    }

    public static Place getDummyPlace(){
        Place place = new Place();
        place.setId(1);
        place.setApartmentNumber(1);
        place.setStreet("Klonowa");
        place.setHomeNumber(21);
        place.setPostalCode("22-523");
        place.setCity("Kielce");
        return place;
    }

    public static ServiceType getDummyServiceType(){
        ServiceType serviceType = new ServiceType();
        serviceType.setId(1L);
        serviceType.setName("One way");
        serviceType.setPriceRate(150);
        return serviceType;
    }


}
