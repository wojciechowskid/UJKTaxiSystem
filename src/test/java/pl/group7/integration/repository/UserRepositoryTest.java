package pl.group7.integration.repository;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.group7.DummyObjects;
import pl.group7.model.entity.User;
import pl.group7.repository.UserRepository;

/**
 * Created by allst on 06.05.2017.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    UserRepository userRepository;

    @Test
    public void shouldPersistUser(){
        User user = DummyObjects.getDummyUser();
        userRepository.save(user);
    }

    @Test
    public void shouldRemovePersistedUser(){
        User user = DummyObjects.getDummyUser();
        userRepository.save(user);
        userRepository.removeById(1);
    }

    @Test
    public void shouldModifyPersistedUser(){
        String phoneNuber = "123456789";
        User user = DummyObjects.getDummyUser();
        userRepository.save(user);
        user.setPhoneNumber(phoneNuber);
        userRepository.save(user);
        User userFromArray = userRepository.findOne(user.getId());
        Assert.assertEquals(userFromArray.getPhoneNumber(),phoneNuber);

    }

    @Test
    public void shouldFindByLoginPersistedUser(){
        User user = DummyObjects.getDummyUser();
        user = userRepository.save(user);

        User persistedUser = userRepository.findByLogin(user.getLogin());

        Assert.assertEquals(user,persistedUser);
    }

}