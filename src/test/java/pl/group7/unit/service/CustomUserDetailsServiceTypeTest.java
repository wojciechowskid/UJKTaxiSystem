package pl.group7.unit.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit4.SpringRunner;
import pl.group7.DummyObjects;
import pl.group7.model.entity.User;
import pl.group7.service.data.UserService;
import pl.group7.service.security.CustomUserDetailsService;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;

/**
 * Created by allst on 06.05.2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomUserDetailsServiceTypeTest {

    @MockBean(UserService.class)
    UserService userService;

    @Autowired
    CustomUserDetailsService userDetailsService;

    private User user = DummyObjects.getDummyUser();

    @Test(expected = UsernameNotFoundException.class)
    public void shouldNotReturnUserDetails() {
        given(userService.findByLogin(user.getLogin())).willReturn(null);
        userDetailsService.loadUserByUsername(user.getLogin());
    }

    @Test
    public void shouldReturnUserDetails() {
        given(userService.findByLogin(user.getLogin())).willReturn(user);
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("SU"));
        org.springframework.security.core.userdetails.User userDetail = new org.springframework.security.core.userdetails.User(user
                .getLogin(), user.getPassword(), true, true, true, true, authorities);

        Assert.assertEquals(userDetail, userDetailsService.loadUserByUsername(user.getLogin()));
    }

}
