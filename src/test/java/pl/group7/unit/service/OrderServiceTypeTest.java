package pl.group7.unit.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import pl.group7.DummyObjects;
import pl.group7.model.entity.Order;
import pl.group7.model.entity.User;
import pl.group7.repository.OrderRepository;
import pl.group7.service.data.OrderService;
import pl.group7.service.messaging.MailContentHelper;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;

/**
 * Created by allst on 06.05.2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderServiceTypeTest {

    @Autowired
    OrderService orderService;
    @MockBean(OrderRepository.class)
    OrderRepository orderRepository;
    @MockBean(MailContentHelper.class)
    MailContentHelper mailContentHelper;

    private User user = DummyObjects.getDummyUser();
    private Order order = DummyObjects.getDummyOrder();

    @Test
    public void shouldPersistOrder(){
        given(this.orderRepository.save(order)).willReturn(order);
        given(this.orderRepository.findOne(1L)).willReturn(null);
        doNothing().when(this.mailContentHelper).sendOrderMade(null);
        given(this.mailContentHelper.getDataForOrderMadeMail(null)).willReturn(null);
        Order persistedOrder = orderService.save(order);
        User userFromOrder = persistedOrder.getCustomer();
//        Assert.assertEquals(user,userFromOrder);
        Assert.assertEquals(order,persistedOrder);
    }

    @Test(expected = EntityExistsException.class)
    public void shouldNotPersistOrder() {
        given(this.orderRepository.findOne(1L)).willReturn(order);
        given(this.orderRepository.save(order)).willReturn(order);
        orderService.save(order);
    }

    @Test
    public void shouldRemoveOrder() {
        given(this.orderRepository.findOne(1L)).willReturn(order);
        orderService.removeById(1L);
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldNotRemoveOrder() throws Exception {
        given(this.orderRepository.findOne(1L)).willReturn(null);
        orderService.removeById(1L);
    }

    @Test
    public void shouldModifyOrder() {
        Order modifiedOrder = DummyObjects.getModifiedOrder();
        given(this.orderRepository.findOne(1L)).willReturn(order);
        given(this.orderRepository.save(modifiedOrder)).willReturn(modifiedOrder);
        Assert.assertEquals(modifiedOrder,orderService.modify(modifiedOrder));
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldNotModifyOrder() throws Exception {
        Order modifiedOrder = DummyObjects.getModifiedOrder();
        given(this.orderRepository.findOne(1L)).willReturn(null);
        orderService.modify(modifiedOrder);
    }

    @Test
    public void shouldFindOrderById() {
        given(this.orderRepository.findOne(1L)).willReturn(order);
        Assert.assertEquals(order,orderService.getById(1L));
    }

    @Test
    public void shouldNotFindOrderById() throws Exception {
        given(this.orderRepository.findOne(1L)).willReturn(null);
        Assert.assertEquals(null,orderService.getById(1L));
    }

    @Test
    public void shouldFindAllOrdersForUser() {
        List<Order> orders = new ArrayList<>();
        orders.add(order);
        given(this.orderRepository.findOrdersByCustomerOrderByOrderDateDesc(user)).willReturn(orders);
        Assert.assertEquals(orders,orderService.findAllOrdersForUser(user));
    }

}
