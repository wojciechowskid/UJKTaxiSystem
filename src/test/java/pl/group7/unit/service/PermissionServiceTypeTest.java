package pl.group7.unit.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.junit4.SpringRunner;
import pl.group7.model.data.Permission;
import pl.group7.service.security.PermissionService;

import java.util.ArrayList;

/**
 * Created by allst on 03.05.2017.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class PermissionServiceTypeTest {

    @Autowired
    PermissionService permissionService;

    @Test
    public void testIfPrincipalHasProperPermission() {
        ArrayList<SimpleGrantedAuthority> auth = new ArrayList<>();
        auth.add(new SimpleGrantedAuthority("SU"));
        UsernamePasswordAuthenticationToken principal = new UsernamePasswordAuthenticationToken("principal", "admin", auth);
        Assert.assertEquals(true,permissionService.hasPermission(principal, Permission.SU));
    }

    @Test
    public void shouldNotReturnTrueBecauseOfIncorrectPermission(){
        ArrayList<SimpleGrantedAuthority> auth = new ArrayList<>();
        auth.add(new SimpleGrantedAuthority("USER"));
        UsernamePasswordAuthenticationToken principal = new UsernamePasswordAuthenticationToken("principal", "admin", auth);
        Assert.assertEquals(false,permissionService.hasPermission(principal, Permission.SU));

    }
}
