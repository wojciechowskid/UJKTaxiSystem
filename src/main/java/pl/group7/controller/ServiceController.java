package pl.group7.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.group7.model.entity.ServiceType;
import pl.group7.service.data.ServiceTypeService;

import java.util.List;

/**
 * Created by allst on 27.05.2017.
 */
@RestController
@RequestMapping(value = "/service")
public class ServiceController {

    private ServiceTypeService typeService;

    @Autowired
    public ServiceController(ServiceTypeService typeService){
        this.typeService = typeService;
    }

    @RequestMapping(value = "/available", method = RequestMethod.GET)
    public List<ServiceType> getOrderById() {
        return typeService.getAllServicesTypes();
    }

    @RequestMapping(value ="/add", method = RequestMethod.POST)
    public ServiceType addService(@RequestBody ServiceType serviceType){
        return typeService.addServiceType(serviceType);
    }


}
