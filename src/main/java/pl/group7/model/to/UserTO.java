package pl.group7.model.to;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.group7.model.data.Role;

import java.util.Date;
import java.util.List;

/**
 * Created by allst on 02.03.2017.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserTO implements TransferObject {

    private long id;
    private String name;
    private String surname;
    private String login;
    private String password;
    private String mailAddress;
    private String phoneNumber;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date joinDate;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date lastLoginDate;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date birthDay;
    private List<Role> roleList;
    private List<Long> ordersMade;
    private List<Long> ordersDispatched;
    private List<Long> ordersAssigned;

}
