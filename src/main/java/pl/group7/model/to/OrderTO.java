package pl.group7.model.to;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.group7.model.data.State;
import pl.group7.model.entity.Place;

import java.util.Date;

/**
 * Created by wojciechowskid on 08/05/2017.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderTO implements TransferObject {

    private long id;
    private long customer;
    private long consultant;
    private long driver;
    private Place source;
    private Place destination;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date orderDate;
    private double distance;
    private State state;
    private Long serviceType;
}
