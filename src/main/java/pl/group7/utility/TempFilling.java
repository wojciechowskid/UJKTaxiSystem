package pl.group7.utility;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;
import pl.group7.model.data.Role;
import pl.group7.model.entity.ServiceType;
import pl.group7.model.entity.User;
import pl.group7.service.data.ServiceTypeService;
import pl.group7.service.data.UserService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by wojciechowskid on 03.03.2017.
 */
@Service
public final class TempFilling implements ApplicationRunner {

    private UserService userService;
    private ServiceTypeService serviceTypeService;

    @Autowired
    public TempFilling(UserService userService,ServiceTypeService serviceTypeService) {
        this.userService = userService;
        this.serviceTypeService = serviceTypeService;
    }

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        List<User> users = new ArrayList<>();
        users.add(new User(1, "Dominik", "Wojciechowski", "Dominik", "Wojciechowski", "doominikwoojciechowski@gmail.com", new Date(), new Date(), new Date(), "666666666", Collections
                .singletonList(Role.USER), new ArrayList<>(), new ArrayList<>(), new ArrayList<>()));
        users.add(new User(2, "Adrian", "Trojanowski", "Adrian", "Trojanowski", "mail@mail.com", new Date(), new Date(), new Date(), "666666666", Collections
                .singletonList(Role.USER), new ArrayList<>(), new ArrayList<>(), new ArrayList<>()));
        users.add(new User(3, "Adam", "Sabat", "Adam", "Sabat", "mail@mail.com", new Date(), new Date(), new Date(), "666666666", Collections
                .singletonList(Role.DBADMIN), new ArrayList<>(), new ArrayList<>(), new ArrayList<>()));
        users.add(new User(4, "admin", "admin", "admin", "admin", "admin@admin", new Date(), new Date(), new Date(), "123123123", Collections
                .singletonList(Role.ADMIN), new ArrayList<>(), new ArrayList<>(), new ArrayList<>()));
        users.add(new User(5, "user", "user", "user", "user", "user@user", new Date(), new Date(), new Date(), "123123123", Collections
                .singletonList(Role.USER), new ArrayList<>(), new ArrayList<>(), new ArrayList<>()));
        users.add(new User(6, "consultant", "consultant", "consultant", "consultant", "user@user", new Date(), new Date(), new Date(), "123123123", Collections
                .singletonList(Role.CONSULTANT), new ArrayList<>(), new ArrayList<>(), new ArrayList<>()));
        users.add(new User(7, "driver", "driver", "driver", "driver", "user@user", new Date(), new Date(), new Date(), "123123123", Collections
                .singletonList(Role.DRIVER), new ArrayList<>(), new ArrayList<>(), new ArrayList<>()));

        users.forEach(user -> userService.save(user));

        List<ServiceType> serviceTypes = new ArrayList<>();
        serviceTypes.add(new ServiceType(1,"Jednorazowe",200));
        serviceTypes.add(new ServiceType(2,"Stałe",150));
        serviceTypes.forEach(serviceType -> serviceTypeService.addServiceType(serviceType));

    }


}

