package pl.group7.utility;

import pl.group7.model.entity.Order;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by allst on 29.05.2017.
 */
public final class CompareUtility {

    public static boolean isAssignable(Order order, Order modifiedOrder){
        boolean result = true;
        if(!(order.getId() == modifiedOrder.getId()))  result=false;
        else if(!(order.getCustomer().equals(modifiedOrder.getCustomer()))) result = false;
        else if(!(order.getSource().equals(modifiedOrder.getSource()))) result = false;
        else if(!(order.getDestination().equals(modifiedOrder.getDestination()))) result = false;
        else if(!(order.getServiceType().equals(modifiedOrder.getServiceType())))result = false;
        else if(!compareDates(order.getOrderDate(),modifiedOrder.getOrderDate())) result = false;
        return result;
    }

    public static boolean isFinishable(Order order, Order modifiedOrder){
        boolean result = isAssignable(order,modifiedOrder);
        if(!(order.getConsultant().equals(modifiedOrder.getConsultant()))) result = false;
        else if(!(order.getDriver().equals(modifiedOrder.getDriver()))) result = false;
        return result;
    }

    public static boolean compareDates(Date first, Date second){
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        return dateFormat.format(first).equals(dateFormat.format(second));
    }

}
