package pl.group7.service.transfer;

import pl.group7.model.entity.Persistable;
import pl.group7.model.to.TransferObject;

/**
 * Created by allst on 06.03.2017.
 */
public interface TransferService<T extends Persistable, U extends TransferObject> {

    T getObject(U to);

    U getTO(T persist);

}
