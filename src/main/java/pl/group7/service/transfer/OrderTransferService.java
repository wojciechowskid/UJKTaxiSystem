package pl.group7.service.transfer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.group7.model.entity.Order;
import pl.group7.model.to.OrderTO;
import pl.group7.service.data.ServiceTypeService;
import pl.group7.service.data.UserService;

/**
 * Created by wojciechowskid on 08/05/2017.
 */
@Service
public class OrderTransferService implements TransferService<Order,OrderTO> {

    @Autowired
    UserService userService;
    @Autowired
    ServiceTypeService typeService;

    @Override
    public Order getObject(OrderTO to) {
        Order order = new Order();
        order.setId(to.getId());
        order.setSource(to.getSource());
        order.setDestination(to.getDestination());
        order.setOrderDate(to.getOrderDate());
        order.setDistance(to.getDistance());
        order.setCustomer(userService.getById(to.getCustomer()));
        order.setConsultant(userService.getById(to.getConsultant()));
        order.setDriver(userService.getById(to.getDriver()));
        order.setState(to.getState());
        order.setServiceType(typeService.getServiceTypeById(to.getServiceType()));
        return order;
    }

    @Override
    public OrderTO getTO(Order persist) {
        OrderTO orderTo = new OrderTO();
        orderTo.setId(persist.getId());
        orderTo.setSource(persist.getSource());
        orderTo.setDestination(persist.getDestination());
        orderTo.setOrderDate(persist.getOrderDate());
        orderTo.setDistance(persist.getDistance());
        if(persist.getCustomer()!= null) {
            orderTo.setCustomer(persist.getCustomer().getId());
        }
        if(persist.getConsultant()!= null) {
            orderTo.setConsultant(persist.getConsultant().getId());
        }
        if(persist.getDriver()!= null) {
            orderTo.setDriver(persist.getDriver().getId());
        }
        if(persist.getServiceType()!= null) {
            orderTo.setServiceType(persist.getServiceType().getId());
        }
        orderTo.setState(persist.getState());
        return orderTo;
    }
}
