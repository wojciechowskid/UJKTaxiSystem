package pl.group7.service.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.group7.model.entity.ServiceType;
import pl.group7.repository.ServiceTypeRepository;

import java.util.List;

/**
 * Created by allst on 27.05.2017.
 */
@Service
public class ServiceTypeService {

    private ServiceTypeRepository serviceTypeRepository;

    @Autowired
    public ServiceTypeService(ServiceTypeRepository serviceTypeRepository){
        this.serviceTypeRepository = serviceTypeRepository;
    }

    public List<ServiceType> getAllServicesTypes(){
        return serviceTypeRepository.findAll();
    }

    public ServiceType getServiceTypeById(long id){
        return serviceTypeRepository.getServiceTypeById(id);
    }

    public ServiceType addServiceType(ServiceType serviceType){
        return serviceTypeRepository.save(serviceType);
    }


}
