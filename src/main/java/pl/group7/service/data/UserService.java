package pl.group7.service.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.group7.model.data.Role;
import pl.group7.model.data.State;
import pl.group7.model.entity.User;
import pl.group7.repository.UserRepository;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by allst on 01.03.2017.
 */
@Service
public class UserService {

    private final Logger logger = LoggerFactory.getLogger(UserService.class);

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User save(User user) {
        if (userRepository.findByLogin(user.getLogin()) == null) {
            try {
                return userRepository.save(user);
            } catch (Exception e) {
                logger.debug(e.getMessage(), e);
                throw e;
            }
        } else {
            EntityExistsException e = new EntityExistsException("This login (" + user.getLogin() + ") already exists in db, find another");
            logger.info(e.getMessage(), e);
        throw e;
        }
    }

    public User getById(long id) {
        return userRepository.findOne(id);
        //TODO LOGGING LOGIC
        //TODO HANDLING EXCEPTIONS
    }

    public boolean removeById(long id) {
        if (userRepository.findOne(id) != null) {
            try {
                userRepository.removeById(id);
                return true;
            } catch (Exception e) {
                logger.debug(e.getMessage(), e);
                throw e;
            }
        }else {
            EntityNotFoundException e = new EntityNotFoundException("There is no such user in DB.");
            logger.info(e.getMessage(), e);
            throw e;
        }
    }

    public User modify(User user) {
        User original = userRepository.findByLogin(user.getLogin());
        if (original != null && original.getId() == user.getId()) {
            try {
                return userRepository.save(user);
            } catch (Exception e) {
                logger.debug(e.getMessage(), e);
                throw e;
            }
        } else {
            EntityNotFoundException e = new EntityNotFoundException("There is no such user in DB.");
            logger.info(e.getMessage(), e);
            throw e;
        }
    }

    public List<User> findAll() {
        return userRepository.findAll();
        //TODO LOGGING LOGIC
        //TODO HANDLING EXCEPTIONS
    }

    public User findByLogin(String login) {
        return userRepository.findByLogin(login);
    }

    public List<User> findAllNonAssignedDrivers(){
        List<User> toFilter  = userRepository.findAllByRoleContains(Role.DRIVER);
        return toFilter.stream()
                .filter(u ->
                        !u.getOrdersAssigned().stream()
                                .filter(o -> o.getState().equals(State.IN_PROCESS)).findFirst().isPresent())
                .collect(Collectors.toList());
    }

}
