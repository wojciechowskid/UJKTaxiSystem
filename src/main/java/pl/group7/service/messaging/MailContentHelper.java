package pl.group7.service.messaging;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import pl.group7.model.entity.Order;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by allst on 18.05.2017.
 */
@Service
public class MailContentHelper {

    private final EmailSender emailSender;
    private final TemplateEngine templateEngine;

    @Autowired
    public MailContentHelper(EmailSender emailSender,
                           TemplateEngine templateEngine){
        this.emailSender = emailSender;
        this.templateEngine = templateEngine;
    }


    public Map<String, String> getDataForOrderMadeMail(Order order){
        Map<String,String> map = new HashMap<>();
        map.put("orderNumber", String.valueOf(order.getId()));
        map.put("userMail",order.getCustomer().getMailAddress());
        return map;
    }

    public void sendOrderMade(Map<String,String> values) {
        Context context = new Context();
        context.setVariable("description", "Tutaj jakis opis...");
        context.setVariable("orderNumber", values.get("orderNumber"));

        String body = templateEngine.process("OrderMadeTemplate", context);
        emailSender.sendEmail(values.get("userMail"), "Order Made : " + values.get("orderNumber"), body);
    }



}
