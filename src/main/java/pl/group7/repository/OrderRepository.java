package pl.group7.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import pl.group7.model.data.State;
import pl.group7.model.entity.Order;
import pl.group7.model.entity.User;

import java.util.List;

/**
 * Created by allst on 04.05.2017.
 */
public interface OrderRepository extends JpaRepository<Order, Long> {

    Order findOne(long id);
    List<Order> findOrdersByCustomerOrderByOrderDateDesc(User user);
    List<Order> findOrdersByState(State state);
    List<Order> findAllByDriverIsNull();
    List<Order> findAllByDriver(User user);
    List<Order> findAllByDriverAndState(User user, State state);
    @Transactional
    void removeById(long id);

}
