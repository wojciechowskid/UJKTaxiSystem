'use strict'; 

var gulp = require('gulp'),
  	webserver = require('gulp-webserver'),
	livereload = require('gulp-livereload'),
	sass = require('gulp-sass'),
	cleanCSS = require('gulp-clean-css'),
  	ngAnnotate = require('gulp-ng-annotate'),
  	uglify = require('gulp-uglify'),
  	closure = require('gulp-jsclosure');

// webserver
gulp.task('webserver', function() {
  gulp.src('static')
    .pipe(webserver({
      	open: true,
      	port: 8080,
      	livereload: true,
      	directoryListing: {
        	enable: true,
        	path: 'static/index.html'
   		}
	}));
});

//livereload
gulp.task('livereload', function() {
  livereload.listen();
  gulp.watch('static/**/*', livereload.reload);
});

// sass compile
gulp.task('sass', function () {
  return gulp.src('static/app-style/app.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('static/'))
});

//sass compile and watch
gulp.task('sass:watch', function () {
  gulp.watch('static/app-style/**/*.scss', ['sass']);
});

//compress css
gulp.task('compress-css', function() {
  return gulp.src('./static/*.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('./static'));
});

// compress js
gulp.task('compress-js', function () {
    return gulp.src('./static/js/**/*.js')
        .pipe(closure({angular: true}))
        .pipe(ngAnnotate())
        .pipe(uglify({mangle: false}))
        .pipe(gulp.dest('dist'));
});


gulp.task('run', ['livereload', 'sass:watch']);