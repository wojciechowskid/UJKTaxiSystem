(function() {
    'use strict';

    angular
        .module("app")
        .controller("userProfileController", userProfileController);

        userProfileController.$inject = ['sessionSrv', 'userSrv', '$state'];

        function userProfileController(sessionSrv, userSrv, $state) {
            var vm = this;

            vm.isLoggedIn = sessionSrv.isLoggedIn;
            vm.logout = logout;
            vm.updateUser = updateUser;
            vm.cancelEdit = cancelEdit;

            sessionSrv.getUser().then(function(res){
                vm.currentUser = res;
            },
            function(reason){
                console.log(reason)
            });

            function logout() {
                vm.currentUser = {};
                sessionSrv.logout();
            }

            //Funkcja updateUser uaktualnia dane edytowanego użytkownika i w przypadku powodzenia przechodzi pod adres (/admin/users)
            function updateUser(user) {
                userSrv.save(user, function(){
                        $state.go('main.home').then(
                            function(){
                                $state.reload()
                            })
                });
            }

            //Funkcja cancelEdit przywraca dane edytowanego użytkownik i w przypadku powodzenia przechodzi pod adres (/admin/users)
            function cancelEdit() {
                $state.go('main.home');
            }
        }
})();