(function() {
    'use strict';

	angular
		.module("app")
		.controller("navigationController", navigationController);

    	navigationController.$inject = ['sessionSrv', '$scope'];

		function navigationController(sessionSrv, $scope) {
            var vm = this;

            vm.currentUser = {};

            vm.logout = logout;

            function logout() {
                vm.currentUser = {};
                sessionSrv.logout();

            }

            $scope.$watchCollection(function(){
                return sessionSrv.isLoggedIn();
            }, function(val){
                vm.isLoggedIn = val;
                if(vm.isLoggedIn) {
                    sessionSrv.getUser().then(function(res) {
                        vm.currentUser = res;

                        var roleList = vm.currentUser.roleList;

                        if(roleList.indexOf('ADMIN') > -1) {
                            vm.role = 'ADMIN';
                        } else if(roleList.indexOf('DBADMIN') > -1) {
                            vm.role = 'DBADMIN';
                        } else if(roleList.indexOf('CONSULTANT') > -1) {
                            vm.role = 'CONSULTANT';
                        } else if(roleList.indexOf('DRIVER') > -1) {
                            vm.role = 'DRIVER';
                        } else {
                            vm.role = 'USER';
                        }
                        sessionSrv.setRole(vm.role);
                    }, function(reason){
                        console.log(reason);
                    });
                }
            });
        }
})();