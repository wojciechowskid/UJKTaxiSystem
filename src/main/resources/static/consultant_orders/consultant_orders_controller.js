(function() {
    'use strict';

    angular
        .module("app")
        .controller("consultantOrdersController", consultantOrdersController);

        consultantOrdersController.$inject = ['orderSrv'];

        function consultantOrdersController(orderSrv) {
            var vm = this;
            vm.orders = {};
            vm.drivers = {};
            vm.driver = "";

            vm.selectDriver = selectDriver;
            vm.submitDriver = submitDriver;


            function getOrders(){
                vm.orders = orderSrv.consultantOrders(function(){
                    vm.drivers = orderSrv.availableDrivers();
                });
            }

            function selectDriver(driver) {
                vm.driver = driver;
            }

            function submitDriver(order){
                console.log(order);

                order.driver = vm.driver;
                orderSrv.submitDriver(order, function () {
                    getOrders();
                });
            }

            getOrders();
        }
})();