(function() {
    'use strict';

    angular
        .module("app")
        .controller("userOrdersController", userOrdersController);

        userOrdersController.$inject = ['orderSrv'];

        function userOrdersController(orderSrv) {
            var vm = this;

            vm.orders = {};
            vm.orders = orderSrv.userOrders();
        }
})();