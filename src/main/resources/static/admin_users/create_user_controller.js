(function() {
    'use strict';

    angular
        .module('app')
        .constant('roles', ['ADMIN', 'USER', 'DBADMIN', 'CONSULTANT', 'DRIVER'])
        .controller("createUserController",  createUserController);

        createUserController.$inject = ['userSrv', '$state', 'roles'];

        function createUserController(userSrv, $state, roles) {

            var vm = this;

            vm.userRoles = roles;
            vm.newUser = {
                roleList: [],
                orders: []

            };

            vm.roleExists = roleExists;
            vm.selectRole = selectRole;
            vm.createUser = createUser;
            vm.cancelCreate = cancelCreate;

            // Funkcja roleExist sprawdza czy wybrana rola znajduje się już w tablicy newUser.role
            function roleExists(role) {
                return vm.newUser.roleList.indexOf(role) > -1;
            }

            // Funkcja selectRole usuwa role z tablicy newUser.role lub ją do niej dodaje jeśli w tablicy nie ma takiej roli
            function selectRole(role) {
                var idx = vm.newUser.roleList.indexOf(role);
                if (idx > -1) {
                    vm.newUser.roleList.splice(idx, 1);
                }
                else {
                    vm.newUser.roleList.push(role);
                }
            }

            // Funkcja createUser wysyła do serwera zapytanie POST z danymi w celu storzenia nowego użytkownika, w przykadku powodzenia uaktualnia tablicę obiektów users i przekierowuje na adres ('/admin/users');
            function createUser(user) {
                new userSrv(user).$create().then( function(newUser) {
                    $state.go('user.adminUsers');
                });
            }

            //Funkcja cancelCreate czyści obiekt newUser i przekierowuje na adres ('/admin/users');
             function cancelCreate() {
                vm.newUser = {};
                $state.go('user.adminUsers');
            }
        }
})();