(function() {
    'use strict';

    angular
        .module('app')
        .constant('roles', ['ADMIN', 'USER', 'DBADMIN', 'CONSULTANT', 'DRIVER'])
        .controller("editUserController", editUserController);

        editUserController.$inject = ['userSrv', '$stateParams', '$state', 'roles'];

        function editUserController(userSrv, $stateParams, $state, roles){

            var vm = this;

            vm.userRoles = roles;
            vm.currentUser = {
                roleList: []
            };

            vm.roleExists = roleExists;
            vm.selectRole =  selectRole;
            vm.updateUser =  updateUser;
            vm.cancelEdit = cancelEdit;

            //Pobranie użytkowników w postaci tablicy obietów resource i przypisanie jej do zmiennej users
            // Pętla forEach wewnątrz, której do zmiennej currentUser zostają przypisane dane użytkownika, na które wskazuje $stateParams.id
            vm.users = userSrv.query(function(){
                angular.forEach(vm.users, function (value) {
                    if (value.id == $stateParams.id) {
                        vm.currentUser = value;
                    }
                });
            });

            // Funkcja roleExist sprawdza czy wybrana rola znajduje się już w tablicy currentUser.role
            function roleExists(role) {
                return vm.currentUser.roleList.indexOf(role) > -1;
            }

            // Funkcja selectRole usuwa role z tablicy currentUser.role lub ją do niej dodaje jeśli w tablicy nie ma takiej roli
            function selectRole(role) {
                var idx = vm.currentUser.roleList.indexOf(role);
                if (idx > -1) {
                    vm.currentUser.roleList.splice(idx, 1);
                }
                else {
                    vm.currentUser.roleList.push(role);
                }
            }

            //Funkcja updateUser uaktualnia dane edytowanego użytkownika i w przypadku powodzenia przechodzi pod adres (/admin/users)
            function updateUser(user) {
                user.$save().then(function() {
                    $state.go('user.adminUsers');
                });
            }

            //Funkcja cancelEdit przywraca dane edytowanego użytkownik i w przypadku powodzenia przechodzi pod adres (/admin/users)
             function cancelEdit() {
                vm.currentUser.$get().then(function(){
                    $state.go('user.adminUsers');
                });
            }
        }
})();