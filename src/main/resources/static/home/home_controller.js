(function() {
    'use strict';

    angular
        .module("app")
        .controller("homeController", homeController);

        homeController.$inject = ['sessionSrv', 'orderSrv', '$timeout'];

        function homeController(sessionSrv, orderSrv, $timeout) {
            var vm = this;

            vm.isLoggedIn = sessionSrv.isLoggedIn();
            vm.orderSuccess = null;
            vm.orderSuccessMsg = 'Twoje zamówienie zostało przyjęte.';
            vm.orderError = null;
            vm.orderErrorMsg = '  Podczas składania zamówienia wystąpił problem';
            vm.order = {};

            vm.selectService = selectService;
            vm.sendOrder = sendOrder;

            function getServices(){
                if(vm.isLoggedIn) {
                    vm.services = orderSrv.getServices();
                }
            }

            function selectService(service) {
                vm.order.serviceType = service;
            }

            function sendOrder(){
                new orderSrv.create(vm.order, function(){
                    vm.order = {};
                    vm.orderSuccess = true;
                    $timeout(function() {vm.orderSuccess = null;}, 3000);
                    vm.checked = false;

                }, function(){
                    vm.orderError = true;
                    $timeout(function() {vm.orderError = null;}, 3000);

                });
            }

            getServices();
        }
})();