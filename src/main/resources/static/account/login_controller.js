(function() {
    'use strict';

    angular
        .module('app')
        .controller("loginController", loginController);

        loginController.$inject = ['sessionSrv', '$state'];

        function loginController(sessionSrv, $state) {
            var vm = this;

            vm.account = {};
            vm.loginError = false;
            vm.dataLoading = false;
            vm.login = login;

            function login() {
                vm.dataLoading = true;
                sessionSrv.login(vm.account,
                    function(){
                        $state.go('main.home');
                    },
                    function(){
                        vm.account = {};
                        vm.dataLoading = false;
                        vm.loginError = true;
                        vm.errorMessage = "Błędny login lub hasło, spróbuj ponownie!";
                });
            }
        }
})();